resource "aws_efs_file_system" "freshclam" {
  creation_token = "freshclam"
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-freshclam"
    }
  )
}

resource "aws_efs_mount_target" "freshclam-private-a" {
  file_system_id  = aws_efs_file_system.freshclam.id
  security_groups = [aws_security_group.freshclam.id]
  subnet_id       = aws_subnet.private-a.id
}

resource "aws_efs_mount_target" "freshclam-private-b" {
  file_system_id  = aws_efs_file_system.freshclam.id
  security_groups = [aws_security_group.freshclam.id]
  subnet_id       = aws_subnet.private-b.id
}

resource "aws_efs_access_point" "freshclam" {
  file_system_id = aws_efs_file_system.freshclam.id
  posix_user {
    gid = "1000"
    uid = "1000"
  }
  root_directory {
    creation_info {
      owner_gid = "1000"
      owner_uid = "1000"
      permissions = "0777"
    }
    path = "/clamav"
  }
}
