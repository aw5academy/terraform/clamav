resource "aws_sqs_queue" "scan" {
  name                      = "${var.app}-scan"
  receive_wait_time_seconds = 20
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.scan-dlq.arn
    maxReceiveCount     = 4
  })
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
  visibility_timeout_seconds = 120
}

resource "aws_sqs_queue_policy" "scan" {
  queue_url = aws_sqs_queue.scan.id
  policy = templatefile("${path.module}/templates/scan-sqs-queue-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      queue_arn      = aws_sqs_queue.scan.arn
    }
  )
}

resource "aws_sqs_queue" "scan-dlq" {
  message_retention_seconds = 604800
  name                      = "${var.app}-scan-dlq"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan-dlq"
    }
  )
}
