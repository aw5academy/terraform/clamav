resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = "true"
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}

################################################
# Gateways
################################################
resource "aws_internet_gateway" "main" {
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_eip" "nat-a" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-a"
    }
  )
  vpc = true
}

resource "aws_eip" "nat-b" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-b"
    }
  )
  vpc = true
}

resource "aws_nat_gateway" "main-a" {
  allocation_id = aws_eip.nat-a.id
  subnet_id     = aws_subnet.public-a.id
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-a"
    }
  )
}

resource "aws_nat_gateway" "main-b" {
  allocation_id = aws_eip.nat-b.id
  subnet_id     = aws_subnet.public-b.id
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-b"
    }
  )
}

################################################
# Subnets
################################################
resource "aws_subnet" "public-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-public-a"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-public-b"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "false"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-private-a"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-private-b"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "protected-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-protected-a"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "protected-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.6.0/24"
  map_public_ip_on_launch = "false"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-protected-b"
    }
  )
  vpc_id = aws_vpc.main.id
}

################################################
# Route Tables
################################################
resource "aws_route_table" "public" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-public"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "public" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
  route_table_id         = aws_route_table.public.id
}

resource "aws_route_table_association" "public-a" {
  route_table_id = aws_route_table.public.id
  subnet_id      = aws_subnet.public-a.id
}

resource "aws_route_table_association" "public-b" {
  route_table_id = aws_route_table.public.id
  subnet_id      = aws_subnet.public-b.id
}

resource "aws_route_table" "private-a" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-private-a"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "private-b" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-private-b"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "private-a" {
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main-a.id
  route_table_id         = aws_route_table.private-a.id
}

resource "aws_route" "private-b" {
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main-b.id
  route_table_id         = aws_route_table.private-b.id
}

resource "aws_route_table_association" "private-a" {
  route_table_id = aws_route_table.private-a.id
  subnet_id      = aws_subnet.private-a.id
}

resource "aws_route_table_association" "private-b" {
  route_table_id = aws_route_table.private-b.id
  subnet_id      = aws_subnet.private-b.id
}

resource "aws_route_table" "protected" {
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-protected"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table_association" "protected-a" {
  route_table_id = aws_route_table.protected.id
  subnet_id      = aws_subnet.protected-a.id
}

resource "aws_route_table_association" "protected-b" {
  route_table_id = aws_route_table.protected.id
  subnet_id      = aws_subnet.protected-b.id
}

################################################
# VPC Endpoints
################################################
resource "aws_vpc_endpoint" "ecr-api" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.${var.region}.ecr.api"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-ecr-api"
    }
  )
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "ecr-dkr" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.${var.region}.ecr.dkr"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-ecr-dkr"
    }
  )
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "sqs" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.${var.region}.sqs"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-sqs"
    }
  )
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "s3" {
  policy = templatefile("${path.module}/templates/s3-endpoint-policy.json.tpl",
    {
      vpcId = aws_vpc.main.id
    }
  )
  route_table_ids = [
                    aws_route_table.private-a.id,
                    aws_route_table.private-b.id,
                    aws_route_table.protected.id
                    ]
  service_name = "com.amazonaws.${var.region}.s3"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-s3"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "logs" {
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints.id]
  service_name        = "com.amazonaws.${var.region}.logs"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-logs"
    }
  )
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_security_group" "endpoints" {
  description = "Security group for VPC endpoints"
  name        = "${var.app}-vpc-endpoints"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-vpc-endpoints"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "endpoints-https" {
  cidr_blocks       = [aws_subnet.private-a.cidr_block, aws_subnet.private-b.cidr_block, aws_subnet.protected-a.cidr_block, aws_subnet.protected-b.cidr_block]
  description       = "HTTPS access from non public subnets in this region"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.endpoints.id
  to_port           = 443
  type              = "ingress"
}
