variable "app" {
  default     = "clamav"
  description = "The string to use for the App tag that will be applied to all created resources."
}

variable "bucket-name" {
  description = "Object creation events in this bucket will be sent to the SQS queue for virus scanning."
}

variable "region" {
  default = "us-east-1"
  description = "The AWS region."
}
