resource "aws_s3_bucket_notification" "scan" {
  bucket = var.bucket-name
  queue {
    queue_arn     = aws_sqs_queue.scan.arn
    events        = ["s3:ObjectCreated:*"]
  }
}

resource "aws_s3_bucket_policy" "scan" {
  bucket = var.bucket-name
  policy = templatefile("${path.module}/templates/bucket-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      bucket-name    = var.bucket-name
      scan_role_arn  = aws_iam_role.scan.arn
      scan_role_id   = aws_iam_role.scan.unique_id
    }
  )
}
