data "archive_file" "scan-metrics" {
  output_path = ".lambda/scan-metrics.zip"
  source_dir  = "lambda-src/scan-metrics"
  type        = "zip"
}

resource "aws_lambda_function" "scan-metrics" {
  environment {
    variables = {
      SCAN_QUEUE_URL              = aws_sqs_queue.scan.id
      ECS_CLUSTER_NAME            = aws_ecs_cluster.scan.name
      ECS_SERVICE_NAME            = aws_ecs_service.scan.name
      CLOUDWATCH_METRIC_NAMESPACE = var.app
      CLOUDWATCH_METRIC_NAME      = local.scan-metric-name
    }
  }
  filename         = ".lambda/scan-metrics.zip"
  function_name    = "${var.app}-scan-metrics"
  handler          = "lambda_function.lambda_handler"
  role             = aws_iam_role.scan-metrics.arn
  runtime          = "python3.7"
  source_code_hash = data.archive_file.scan-metrics.output_base64sha256
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan-metrics"
    }
  )
  timeout          = "30"
}
