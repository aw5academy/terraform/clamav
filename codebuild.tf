resource "aws_security_group" "codebuild" {
  description = "CodeBuild security group"
  name        = "${var.app}=codebuild"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}=codebuild"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "codebuild-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.codebuild.id
  to_port           = 0
  type              = "egress"
}

resource "aws_codebuild_project" "ecr" {
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_REGISTRY_HOST"
      value = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com"
    }
    environment_variable {
      name = "AWS_REGISTRY_REPO"
      value = aws_ecr_repository.ecr.name
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.ecr.name
    }
  }
  name          = "ecr-${var.app}"
  service_role  = aws_iam_role.codebuild.arn
  source {
    type     = "CODECOMMIT"
    location = aws_codecommit_repository.ecr.clone_url_http
  }
  tags = merge(local.common-tags,
    {
      Name = "ecr-${var.app}"
    }
  )
  vpc_config {
    vpc_id = aws_vpc.main.id
    subnets = [aws_subnet.private-a.id, aws_subnet.private-b.id]
    security_group_ids = [aws_security_group.codebuild.id]
  }
}
