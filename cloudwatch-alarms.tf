resource "aws_cloudwatch_metric_alarm" "scan-scale-out" {
  alarm_actions       = [aws_appautoscaling_policy.scan-scale-out.arn]
  alarm_name          = "${var.app}-scan-scale-out"
  alarm_description   = "Alarms when our queue backlog per task is greater than zero."
  comparison_operator = "GreaterThanThreshold"
  datapoints_to_alarm = "1"
  evaluation_periods  = "1"
  metric_name         = local.scan-metric-name
  namespace           = var.app
  period              = "60"
  statistic           = "Sum"
  dimensions = {
    ECS-Service = aws_ecs_service.scan.name
  }
  threshold          = "0"
  treat_missing_data = "breaching"
}

resource "aws_cloudwatch_metric_alarm" "scan-scale-in" {
  alarm_actions       = [aws_appautoscaling_policy.scan-scale-in.arn]
  alarm_name          = "${var.app}-scan-scale-in"
  alarm_description   = "Alarms when our 5min average queue backlog per task is less than 200."
  comparison_operator = "LessThanThreshold"
  datapoints_to_alarm = "1"
  evaluation_periods  = "1"
  metric_name         = local.scan-metric-name
  namespace           = var.app
  period              = "300"
  statistic           = "Average"
  dimensions = {
    ECS-Service = aws_ecs_service.scan.name
  }
  threshold          = "200"
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "scan-old-messages" {
  alarm_actions       = [aws_sns_topic.alarm.arn]
  alarm_name          = "${var.app}-scan-old-messages"
  alarm_description   = "Alarms when the approx. age of oldest message is greater than 900s."
  comparison_operator = "GreaterThanThreshold"
  datapoints_to_alarm = "1"
  evaluation_periods  = "1"
  metric_name         = "ApproximateAgeOfOldestMessage"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Sum"
  dimensions = {
    QueueName = aws_sqs_queue.scan.name
  }
  ok_actions         = [aws_sns_topic.alarm.arn]
  threshold          = "900"
  treat_missing_data = "breaching"
}

resource "aws_cloudwatch_metric_alarm" "scan-dlq" {
  alarm_actions       = [aws_sns_topic.alarm.arn]
  alarm_name          = "${var.app}-scan-dlq"
  alarm_description   = "Alarms when we have 1 or more messages in the scan SQS DLQ."
  comparison_operator = "GreaterThanThreshold"
  datapoints_to_alarm = "1"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Sum"
  dimensions = {
    QueueName = aws_sqs_queue.scan-dlq.name
  }
  ok_actions         = [aws_sns_topic.alarm.arn]
  threshold          = "0"
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "scan-infected" {
  name           = "scan-infected"
  pattern        = "\"av-status=INFECTED\""
  log_group_name = aws_cloudwatch_log_group.scan.name
  metric_transformation {
    name      = "InfectedFiles"
    namespace = var.app
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "scan-infected" {
  alarm_actions       = [aws_sns_topic.alarm.arn]
  alarm_name          = "${var.app}-scan-infected"
  alarm_description   = "Alarms if an infected file is found."
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "InfectedFiles"
  namespace           = var.app
  ok_actions          = [aws_sns_topic.alarm.arn]
  period              = "60"
  statistic           = "Sum"
  threshold           = "0"
}

resource "aws_cloudwatch_log_metric_filter" "freshclam-updated" {
  name           = "freshclam-updated"
  pattern        = "\"SUCESS! Virus database updated.\""
  log_group_name = aws_cloudwatch_log_group.freshclam.name
  metric_transformation {
    name      = "FreshclamSuccess"
    namespace = var.app
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "freshclam-updated" {
  alarm_actions       = [aws_sns_topic.alarm.arn]
  alarm_name          = "${var.app}-freshclam-updated"
  alarm_description   = "Alarms if freshclam fails to update the virus database."
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "4"
  metric_name         = "FreshclamSuccess"
  namespace           = var.app
  ok_actions          = [aws_sns_topic.alarm.arn]
  period              = "10800"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "breaching"
}
