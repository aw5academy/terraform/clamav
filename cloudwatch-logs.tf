resource "aws_cloudwatch_log_group" "scan" {
  name              = "/aws/ecs/${var.app}-scan"
  retention_in_days = "180"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
}

resource "aws_cloudwatch_log_group" "freshclam" {
  name              = "/aws/ecs/${var.app}-freshclam"
  retention_in_days = "180"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-freshclam"
    }
  )
}

resource "aws_cloudwatch_log_group" "scan-metrics" {
  name              = "/aws/lambda/${var.app}-scan-metrics"
  retention_in_days = "180"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan-metrics"
    }
  )
}

resource "aws_cloudwatch_log_group" "ecr" {
  name              = "/aws/codebuild/ecr-${var.app}"
  retention_in_days = "30"
  tags = merge(local.common-tags,
    {
      Name = "ecr-${var.app}"
    }
  )
}
