################################################
# Freshclam Security Group
################################################
resource "aws_security_group" "freshclam" {
  description = "${var.app}-freshclam"
  name        = "${var.app}-freshclam"
  tags        = merge(local.common-tags,
    {
      Name = "${var.app}-freshclam"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "freshclam-ingress" {
  description              = "Allowing EFS access"
  from_port                = "2049"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.freshclam.id
  source_security_group_id = aws_security_group.freshclam.id
  to_port                  = "2049"
  type                     = "ingress"
}

resource "aws_security_group_rule" "freshclam-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.freshclam.id
  to_port           = 0
  type              = "egress"
}

################################################
# Clamscan Security Group
################################################
resource "aws_security_group" "scan" {
  description = "${var.app}-scan"
  name        = "${var.app}-scan"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "scan-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.scan.id
  to_port           = 0
  type              = "egress"
}
