resource "aws_ecs_cluster" "scan" {
  name = var.app
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}

resource "aws_ecs_task_definition" "scan" {
  execution_role_arn       = aws_iam_role.ecs-task-exec-role.arn
  family                   = "${var.app}-scan"
  container_definitions    = templatefile("${path.module}/templates/scan-container-definitions.json.tpl", 
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      imageTag       = "latest"
      region         = var.region
      queueUrl       = aws_sqs_queue.scan.id
      logGroupName   = aws_cloudwatch_log_group.scan.name
      sqsMaxMessages = "2" # For optimal performance, keep this roughly equal to your vCPU count
    }
  )
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 4096
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
  task_role_arn            = aws_iam_role.scan.arn
  volume {
    name = "freshclam"

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.freshclam.id
      transit_encryption      = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.freshclam.id
        iam             = "ENABLED"
      }
    }
  }
}

resource "aws_ecs_service" "scan" {
  name                              = "scan"
  cluster                           = aws_ecs_cluster.scan.id
  task_definition                   = aws_ecs_task_definition.scan.arn
  desired_count                     = 0
  enable_ecs_managed_tags           = true
  launch_type                       = "FARGATE"
  lifecycle {
    ignore_changes = [desired_count]
  }
  propagate_tags                    = "TASK_DEFINITION"
  platform_version                  = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.scan.id, aws_security_group.freshclam.id]
    subnets         = [aws_subnet.protected-a.id, aws_subnet.protected-b.id]
  }

  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
}

resource "aws_ecs_task_definition" "freshclam" {
  execution_role_arn       = aws_iam_role.ecs-task-exec-role.arn
  family                   = "${var.app}-freshclam"
  container_definitions    = templatefile("${path.module}/templates/freshclam-container-definitions.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      imageTag       = "latest"
      region         = var.region
      logGroupName   = aws_cloudwatch_log_group.freshclam.name
    }
  )
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 4096
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-freshclam"
    }
  )
  task_role_arn            = aws_iam_role.scan.arn
  volume {
    name = "freshclam"

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.freshclam.id
      transit_encryption      = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.freshclam.id
        iam             = "ENABLED"
      }
    }
  }
}
