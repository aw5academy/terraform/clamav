resource "aws_codecommit_repository" "ecr" {
  repository_name = var.app
  tags = merge(local.common-tags,
    {
      Name = "ecr-${var.app}"
    }
  )
}
