import json
import boto3
import os

SCAN_QUEUE_URL = os.getenv('SCAN_QUEUE_URL')
ECS_CLUSTER_NAME = os.getenv('ECS_CLUSTER_NAME')
ECS_SERVICE_NAME = os.getenv('ECS_SERVICE_NAME')
CLOUDWATCH_METRIC_NAMESPACE = os.getenv('CLOUDWATCH_METRIC_NAMESPACE')
CLOUDWATCH_METRIC_NAME = os.getenv('CLOUDWATCH_METRIC_NAME')
AWS_REGION = os.getenv('AWS_REGION')
SQS = boto3.client('sqs', region_name=AWS_REGION)
ECS = boto3.client('ecs', region_name=AWS_REGION)
CW = boto3.client('cloudwatch', region_name=AWS_REGION)

def lambda_handler(event, context):
  approxNumberOfMessages = getApproxNumberofMessages()
  runningTasks = getRunningTasks()
  putMetricData(approxNumberOfMessages, runningTasks)
  return {
    'statusCode': 200,
    'body': json.dumps('')
  }

def getApproxNumberofMessages():
  response = SQS.get_queue_attributes(
    QueueUrl=SCAN_QUEUE_URL,
    AttributeNames=['ApproximateNumberOfMessages']
  )['Attributes']['ApproximateNumberOfMessages']
  return float(response)

def getRunningTasks():
  response = ECS.describe_services(
    cluster=ECS_CLUSTER_NAME,
    services=[
        ECS_SERVICE_NAME,
    ],
  )['services'][0]['runningCount']
  return float(response)

def putMetricData(approxNumberOfMessages, runningTasks):
  if runningTasks > 0.0:
    metricValue = approxNumberOfMessages / runningTasks
  else:
    metricValue = approxNumberOfMessages
  response = CW.put_metric_data(
    Namespace=CLOUDWATCH_METRIC_NAMESPACE,
    MetricData=[
        {
            'MetricName': CLOUDWATCH_METRIC_NAME,
            'Dimensions': [
                {
                    'Name': 'ECS-Service',
                    'Value': ECS_SERVICE_NAME
                },
            ],
            'Value': metricValue,
            'Unit': 'None',
        },
    ]
  )
