{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Action": [
        "sqs:DeleteMessage",
        "sqs:ReceiveMessage"
      ],
      "Resource": "arn:aws:sqs:*:${aws_account_id}:${sqs_queue_name}"
    },
    {
      "Action":[
        "kms:Decrypt"
      ],
      "Effect":"Allow",
      "Resource":[
        "arn:aws:kms:*:${aws_account_id}:key/*"
      ],
      "Condition": {
        "StringEquals": {
          "aws:ResourceTag/Name": [
            "sqs"
          ]
        }
      }
    }
  ]
}
