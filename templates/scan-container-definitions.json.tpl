[
  {
    "name": "scan",
    "image": "${aws_account_id}.dkr.ecr.${region}.amazonaws.com/clamav:${imageTag}",
    "essential": true,
    "environment": [
      {"name": "QUEUE_URL", "value": "${queueUrl}"},
      {"name": "AWS_REGION", "value": "${region}"},
      {"name": "START_SCRIPT", "value": "clamdscan.sh"},
      {"name": "SQS_MAX_RECEIVE_MESSAGES", "value": "${sqsMaxMessages}"}
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${logGroupName}",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "scan"
      }
    },
    "mountPoints": [
      {
        "readOnly": true,
        "containerPath": "/mnt/efs",
        "sourceVolume": "freshclam"
      }
    ]
  }
]
