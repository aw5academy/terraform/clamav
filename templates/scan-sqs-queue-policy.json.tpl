{
 "Version": "2012-10-17",
 "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${aws_account_id}:root"
      },
      "Action": "SQS:*",
      "Resource": "${queue_arn}"
    },
  {
   "Effect": "Allow",
   "Principal": {
     "Service": "s3.amazonaws.com"  
   },
   "Action": [
    "SQS:SendMessage"
   ],
   "Resource": "${queue_arn}",
   "Condition": {
      "ArnLike": { "aws:SourceArn": "arn:aws:s3:*:*:*" },
      "StringEquals": { "aws:SourceAccount": "${aws_account_id}" }
   }
  }
 ]
}
