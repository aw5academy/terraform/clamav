{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${scan_role_arn}"
      },
      "Action": [
        "s3:DeleteObject",
        "s3:GetObject",
        "s3:GetObjectTagging",
        "s3:GetObjectVersion",
        "s3:ListBucket",
        "s3:PutObjectTagging",
        "s3:PutObjectVersionTagging"
      ],
      "Resource": [
        "arn:aws:s3:::${bucket-name}",
        "arn:aws:s3:::${bucket-name}/*"
      ]
    },
    {
      "Effect": "Deny",
      "Principal": "*",
      "Action": [
        "s3:GetObject",
        "s3:PutObjectTagging"
      ],
      "Resource": "arn:aws:s3:::${bucket-name}/*",
      "Condition": {
        "StringNotLike": {
          "aws:userid": [
            "${scan_role_id}:*",
            "${aws_account_id}"
          ]
        },
        "StringNotEquals": {
          "s3:ExistingObjectTag/av-status": "CLEAN"
        }
      }
    }
  ]
}
