[
  {
    "name": "freshclam",
    "image": "${aws_account_id}.dkr.ecr.${region}.amazonaws.com/clamav:${imageTag}",
    "essential": true,
    "environment": [
      {"name": "START_SCRIPT", "value": "freshclam.sh"}
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${logGroupName}",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "freshclam"
      }
    },
    "mountPoints": [
      {
        "readOnly": false,
        "containerPath": "/mnt/efs",
        "sourceVolume": "freshclam"
      }
    ]
  }
]
