################################################
# Freshclam Event
################################################
resource "aws_cloudwatch_event_rule" "freshclam" {
  name                = "${var.app}-freshclam"
  description         = "Starts the freshclam task"
  schedule_expression = "rate(3 hours)"
}

resource "aws_cloudwatch_event_target" "freshclam" {
  target_id = "freshclam"
  arn       = aws_ecs_cluster.scan.arn
  rule      = aws_cloudwatch_event_rule.freshclam.name
  role_arn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsEventsRole"
  ecs_target {
    task_count          = 1
    task_definition_arn = aws_ecs_task_definition.freshclam.arn
    launch_type         = "FARGATE"
    platform_version    = "1.4.0"
    network_configuration {
      subnets          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
      security_groups  = [aws_security_group.freshclam.id]
    }
  }
}

################################################
# Scan Metrics Event
################################################
resource "aws_cloudwatch_event_rule" "scan-metrics" {
  name                = "${var.app}-scan-metrics"
  description         = "Invokes the scan-metrics function"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "scan-metrics" {
  arn  = aws_lambda_function.scan-metrics.arn
  rule = aws_cloudwatch_event_rule.scan-metrics.name
}

resource "aws_lambda_permission" "scan-metrics" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.scan-metrics.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.scan-metrics.arn
  statement_id  = "AllowExecutionFromCloudWatch"
}
