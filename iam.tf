data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}


data "aws_iam_policy_document" "ecs-tasks" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

################################################
# Clamscan Role
################################################
resource "aws_iam_role" "scan" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${var.app}-scan"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan"
    }
  )
}

resource "aws_iam_role_policy" "scan" {
  name   = "scan"
  policy = templatefile("${path.module}/templates/scan-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      sqs_queue_name = aws_sqs_queue.scan.name
      log_group_name = aws_cloudwatch_log_group.scan.name
    }
  )
  role   = aws_iam_role.scan.id
}

################################################
# Scan Metrics Role
################################################
resource "aws_iam_role" "scan-metrics" {
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  name               = "${var.app}-scan-metrics"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-scan-metrics"
    }
  )
}

resource "aws_iam_role_policy" "scan-metrics" {
  name   = "scan-metrics"
  policy = templatefile("${path.module}/templates/scan-metrics-policy.json.tpl",
    {
      aws_account_id   = data.aws_caller_identity.current.account_id
      function-name    = "${var.app}-scan-metrics"
      scan_queue_name  = aws_sqs_queue.scan.name
    }
  )
  role   = aws_iam_role.scan-metrics.id
}

################################################
# ECR CodeBuild Role
################################################
data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "${var.app}-ecr-codebuild"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-ecr-codebuild"
    }
  )
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "codebuild"
  policy = templatefile("${path.module}/templates/codebuild-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      region         = var.region
    }
  )
  role   = aws_iam_role.codebuild.id
}



################################################
# ECS Task Execution Role
################################################
data "aws_iam_policy" "ecs-task-exec-role-policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role" "ecs-task-exec-role" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${var.app}-ecsTaskExecutionRole"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-ecsTaskExecutionRole"
    }
  )
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-role-policy" {
  policy_arn = data.aws_iam_policy.ecs-task-exec-role-policy.arn
  role       = aws_iam_role.ecs-task-exec-role.name
}
