resource "aws_appautoscaling_target" "scan" {
  max_capacity       = 8
  min_capacity       = 0
  resource_id        = "service/${aws_ecs_cluster.scan.name}/${aws_ecs_service.scan.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "scan-scale-out" {
  name               = "${var.app}-scan-scale-out"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.scan.resource_id
  scalable_dimension = aws_appautoscaling_target.scan.scalable_dimension
  service_namespace  = aws_appautoscaling_target.scan.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
        metric_interval_upper_bound = "40"
        scaling_adjustment          = 2
    }
    step_adjustment {
        metric_interval_lower_bound = "40"
        metric_interval_upper_bound = "80"
        scaling_adjustment          = 4
    }
    step_adjustment {
        metric_interval_lower_bound = "80"
        scaling_adjustment          = 8
    }
  }
}

resource "aws_appautoscaling_policy" "scan-scale-in" {
  name               = "${var.app}-scan-scale-in"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.scan.resource_id
  scalable_dimension = aws_appautoscaling_target.scan.scalable_dimension
  service_namespace  = aws_appautoscaling_target.scan.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
