output "scan-queue-arn" {
  value = aws_sqs_queue.scan.arn
}

output "scan-role-arn" {
  value = aws_iam_role.scan.arn
}

output "scan-role-unique_id" {
  value = aws_iam_role.scan.unique_id
}
