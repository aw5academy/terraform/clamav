# clamav
The [ClamAV](https://www.clamav.net/) Terraform code. A serverless anti-virus solution for objects delivered to S3 buckets.

This deploys an ECS Fargate cluster and an EFS file system. A scheduled Fargate tasks runs every 3 hours which runs the the `freshclam` utility to update the virus definitions. These definitions are saved to the EFS file system.

Also deployed is an SQS queue and an ECS Fargate service. The Fargate service reads the messages in the SQS queue which, consist of S3 bucket notifications containing details about the S3 object. The service will download the object, perform a virus scan using the `clamdscan` utility and tag the S3 object with the result of the scan. 

# Docker Code
The repo at [aw5academy/docker/clamav](https://gitlab.com/aw5academy/docker/clamav) contains the Docker code for the Fargate tasks that run in the ECS cluster.

# Configuration
This section explains how you can configure an S3 bucket to have files scanned on upload.

## Bucket Policy
Firstly, the bucket policy needs to be updated to allow the scan ECS service access to it. The bucket policy should also prevent access to unscanned objects. The policy at templates/bucket-policy.json.tpl can be used as a template.

## Bucket Notification
A bucket notification needs to be added to send the event to the SQS queue. The following Terraform code shows how you can do this.
```
data "aws_sqs_queue" "scan" {
  name = "clamav-scan"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id
  queue {
    queue_arn     = data.aws_sqs_queue.scan.arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".log"
  }
}
```
