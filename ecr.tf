resource "aws_ecr_repository" "ecr" {
  name  = var.app
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}
